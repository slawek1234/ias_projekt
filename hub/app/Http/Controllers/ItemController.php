<?php

namespace App\Http\Controllers;
use GuzzleHttp\Client;

class ItemController extends Controller
{
    //
    public function index(){
        $client = new Client();
        $movieResponse = $client->request('GET', 'http://localhost/provider1/movie');
        $filmResponse = $client->request('GET', 'http://localhost/provider2/film');

        $movies = json_decode($movieResponse->getBody(), true);
        $film = json_decode($filmResponse->getBody(), true);
        $items = array_merge($this->convertMovies($movies), $this->convertFilm($film));
        shuffle($items);
        return response()->json($items);
        return $items;

    }
    public function show($id){
        return response()->json([]);
    }

    private function convertMovies($movies){
        $result = [];
        foreach ($movies as $movie){
            $item = [
                'identyfikator' => $movie['id'],
                'tytul' => $movie['movie'],
                'gatunek' => $movie['genre'],
                'rok_produkcji' => $movie['year'],
                'wypozyczalnia' => 'zagraniczna'
            ];
            array_push($result, $item);
        }
        return $result;
    }
    private function convertFilm($film){
        $result = [];
        foreach ($film as $feelm){
            $item = [
                'identyfikator' => $feelm['movieid'],
                'tytul' => $feelm['name'],
                'gatunek' => $feelm['genre'],
                'rok_produkcji' => $feelm['year'],
                'wypozyczalnia' => 'polska'
            ];
            array_push($result, $item);
        }
        return $result;
    }
}
