<?php

namespace App\Http\Controllers;
use App\film;

class FilmController extends Controller
{
    //
    public function index(){
        return response()->json(film::limit(30)->get());
    }
    public function show($id){
        return response()->json(film::find($id));
    }
}
